from flask import Flask, render_template, request
from pymongo import MongoClient

app = Flask(__name__)

# MongoDB connection
client = MongoClient('mongodb://localhost:27017/')
db = client['test_database']
collection = db['test_collection']

@app.route('/', methods=['GET', 'POST'])
def index():
    if request.method == 'POST':
        data = {
            'name': request.form.get('name'),
            'age': request.form.get('age')
        }
        collection.insert_one(data)
        return 'Data stored successfully in MongoDB!'
    return render_template('index.html')

if __name__ == '__main__':
    app.run(debug=True)
